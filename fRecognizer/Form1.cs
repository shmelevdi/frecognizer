﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;
using AForge.Video;
using AForge.Video.DirectShow;

namespace fRecognizer
{
    public partial class Form1 : Form
    {
        private FilterInfoCollection VideoCaptureDevices;
        private VideoCaptureDevice FinalVideo;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in VideoCaptureDevices)
            {
                comboBox1.Items.Add(VideoCaptureDevice.Name);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FinalVideo = new VideoCaptureDevice(VideoCaptureDevices[comboBox1.SelectedIndex].MonikerString);
            FinalVideo.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
            FinalVideo.Start();
        }

        private void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap video = (Bitmap)eventArgs.Frame.Clone();
            ProcessImage(video);
        }

        private System.Drawing.Point[] ToPointsArray(List<IntPoint> points)
        {
            return points.Select(p => new System.Drawing.Point(p.X, p.Y)).ToArray();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (FinalVideo.IsRunning)
            {
                FinalVideo.Stop();
            }
        }

        private void ProcessImage(Bitmap bitmap)
        {
            BitmapData bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, bitmap.PixelFormat);
            ColorFiltering colorFilter = new ColorFiltering();
            colorFilter.Red = new IntRange(0, 128);
            colorFilter.Green = new IntRange(0, 128);
            colorFilter.Blue = new IntRange(0, 128);
            colorFilter.FillOutsideRange = false;
            colorFilter.ApplyInPlace(bitmapData);
            BlobCounter blobCounter = new BlobCounter();
            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 5;
            blobCounter.MinWidth = 5;
            blobCounter.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            bitmap.UnlockBits(bitmapData);
            SimpleShapeChecker shapeChecker = new SimpleShapeChecker();
            Graphics g = Graphics.FromImage(bitmap);
            Pen redPen = new Pen(Color.Red, 2);       // неправильный прямоугольник
            Pen brownPen = new Pen(Color.Brown, 2);   // правильный прямоугольник
            Pen greenPen = new Pen(Color.Green, 2);   // равнобедренный треугольник
            Pen bluePen = new Pen(Color.Blue, 2);     // неправильный треугольник
            Pen pinkPen = new Pen(Color.Pink, 2);     // круг
            for (int i = 0, n = blobs.Length; i < n; i++)
            {               
                List<IntPoint> edgePoints = blobCounter.GetBlobsEdgePoints(blobs[i]);
                AForge.Point center;
                float radius;
                if (shapeChecker.IsCircle(edgePoints, out center, out radius))
                {
                    g.DrawEllipse(pinkPen,
                        (float)(center.X - radius), (float)(center.Y - radius),
                        (float)(radius * 2), (float)(radius * 2));
                    RectangleF rectCircle = new RectangleF((center.X - 30), (center.Y ), 90, 50);
                    g.DrawString("Круг", new Font("Tahoma", 8), Brushes.Red, rectCircle);
                }
                else
                {                   
                    {
                        List<IntPoint> corners;
                        if (shapeChecker.IsConvexPolygon(edgePoints, out corners))
                        {
                            PolygonSubType subType = shapeChecker.CheckPolygonSubType(corners);
                            Pen pen;
                            if (subType == PolygonSubType.Unknown)
                            {
                                if (corners.Count == 4)
                                {
                                    pen = redPen;            
                                    RectangleF rectP1 = new RectangleF(corners[0].X, corners[0].Y, 90, 50);
                                    g.DrawString("Прямоугольник", new Font("Tahoma", 8), Brushes.Red, rectP1);
                                    g.DrawPolygon(pen, ToPointsArray(corners));
                                }
                                else
                                {
                                    pen = bluePen;
                                    RectangleF rectP1 = new RectangleF(corners[0].X, corners[0].Y, 90, 50);
                                    g.DrawString("Треугольник", new Font("Tahoma", 8), Brushes.Red, rectP1);
                                    g.DrawPolygon(pen, ToPointsArray(corners));
                                }
                            }
                            else
                            {
                                if (corners.Count == 4)
                                {
                                    pen = brownPen;
                                    RectangleF rectP1 = new RectangleF(corners[0].X, corners[0].Y, 90, 50);
                                    g.DrawString("Прямоугольник", new Font("Tahoma", 8), Brushes.Red, rectP1);
                                    g.DrawPolygon(pen, ToPointsArray(corners));
                                }
                                else
                                {
                                    pen = greenPen;
                                    RectangleF rectP1 = new RectangleF(corners[0].X, corners[0].Y, 90, 50);
                                    g.DrawString("Треугольник", new Font("Tahoma", 8), Brushes.Red, rectP1);
                                    g.DrawPolygon(pen, ToPointsArray(corners));
                                }
                            }

                           
                        }
                    }
                }
                   
            }

            redPen.Dispose();
            greenPen.Dispose();
            bluePen.Dispose();
            brownPen.Dispose();
            g.Dispose();
            pictureBox1.Image = bitmap;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FinalVideo.IsRunning)
            {
                FinalVideo.Stop();
            }
        }
    }
}
